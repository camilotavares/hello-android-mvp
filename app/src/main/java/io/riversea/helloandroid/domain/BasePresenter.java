package io.riversea.helloandroid.domain;


public interface BasePresenter {
    void destroy();
}
