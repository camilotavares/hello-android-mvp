package io.riversea.helloandroid.application;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class DebugActivity extends AppCompatActivity {
    protected final static String TAG = "Book";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(getClassName(), "onCreate() chamado: " + savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(getClassName(), "onStart() chamado");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(getClassName(), "onRestart() chamado");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(getClassName(), "onResume() chamado");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(getClassName(), "onPause() chamado");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i(getClassName(), "onSaveInstanceState() chamado");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(getClassName(), "onStop() chamado");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(getClassName(), "onDestroy() chamado");
    }

    private String getClassName(){
        String s = getClass().getName();
        return "Book - "+ s.substring(s.lastIndexOf("."));
    }
}
