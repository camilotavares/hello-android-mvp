package io.riversea.helloandroid.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import io.riversea.helloandroid.R;
import io.riversea.helloandroid.WelcomeActivity;
import io.riversea.helloandroid.application.DebugActivity;

public class LoginActivity extends DebugActivity implements LoginContract.View {
    AppCompatButton login;
    EditText usuario, edSenha;
    private LoginContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.capitulo_cinco);
        }

        usuario = findViewById(R.id.usuario);
        edSenha = findViewById(R.id.senha);

        login = findViewById(R.id.login);
        login.setOnClickListener(onLoginClick);

        presenter = new LoginPresenter(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(onSearch());

        return true;
    }

    private SearchView.OnQueryTextListener onSearch() {
        return new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                toastAlert("Busca o texto " + query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.d(TAG, "To query: "+ newText);
                return false;
            }
        };
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                presenter.searchActionClick();
                return true;
            case R.id.action_refresh:
                presenter.refreshActionClick();
                return true;
            case R.id.action_settings:
                presenter.settingsActionClick();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private View.OnClickListener onLoginClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            presenter.handleLoginClick(usuario.getText().toString(), edSenha.getText().toString());
        }
    };

    @Override
    public void showSearchToast() {
        toastAlert("Search Action");
    }

    @Override
    public void showRefreshToast() {
        toastAlert("Refresh Action");
    }

    @Override
    public void showSettingsToast() {
        toastAlert("Settings Action");
    }

    @Override
    public void errorLoginMessage() {
        toastAlert("Login e/ou senha incorretos");
    }

    @Override
    public void startHomeScreen() {
        Intent intent = new Intent(this, WelcomeActivity.class);
        intent.putExtra("nome", "Camilo Tavares");
        startActivity(intent);
    }

    private void toastAlert(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
