package io.riversea.helloandroid.login;


import io.riversea.helloandroid.domain.BasePresenter;

public interface LoginContract {
    interface View {

        void showSearchToast();

        void showRefreshToast();

        void showSettingsToast();

        void errorLoginMessage();

        void startHomeScreen();
    }
    interface Presenter extends BasePresenter {

        void searchActionClick();

        void refreshActionClick();

        void settingsActionClick();

        void handleLoginClick(String login, String password);
    }
}
