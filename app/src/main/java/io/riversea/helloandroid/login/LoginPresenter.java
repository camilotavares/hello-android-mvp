package io.riversea.helloandroid.login;


public class LoginPresenter implements LoginContract.Presenter {
    private LoginContract.View view;

    public LoginPresenter(LoginContract.View view) {
        this.view = view;
    }

    @Override
    public void destroy() {

    }

    @Override
    public void searchActionClick() {
        view.showSearchToast();
    }

    @Override
    public void refreshActionClick() {
        view.showRefreshToast();
    }

    @Override
    public void settingsActionClick() {
        view.showSettingsToast();
    }

    @Override
    public void handleLoginClick(String login, String password) {
        if ("camilo".equals(login) && "123".equals(password)) {
            view.startHomeScreen();
        } else {
            view.errorLoginMessage();
        }
    }
}
