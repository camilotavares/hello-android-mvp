package io.riversea.helloandroid;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import io.riversea.helloandroid.application.DebugActivity;

public class WelcomeActivity extends DebugActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Recebe o nome enviado por parâmetro
        String nome = getIntent().getStringExtra("nome");

        TextView text = findViewById(R.id.texName);
        text.setText(nome + ", seja bem bindo@");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
