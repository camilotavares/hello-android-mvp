package io.riversea.helloandroid.login;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class LoginPresenterTest {

    private LoginContract.Presenter presenter;

    @Mock
    private LoginContract.View view;

    @Before
    public void setUp() {
        presenter = new LoginPresenter(view);
    }

    @Test
    public void handleSearchClick() {
        presenter.searchActionClick();

        verify(view).showSearchToast();
        verifyNoMoreInteractions(view);
    }

    @Test
    public void handleRefreshClick() {
        presenter.refreshActionClick();

        verify(view).showRefreshToast();
        verifyNoMoreInteractions(view);
    }

    @Test
    public void handleSettingsClick() {
        presenter.settingsActionClick();

        verify(view).showSettingsToast();
        verifyNoMoreInteractions(view);
    }

    @Test
    public void showErrorMessageWhenFailedToTryLogin() {
        String invalidLogin = "camilot";
        String invalidPassword = "1234";
        presenter.handleLoginClick(invalidLogin, invalidPassword);

        verify(view).errorLoginMessage();
        verifyNoMoreInteractions(view);
    }

    @Test
    public void showMainScreenWhenLoginIsValid() {
        String validLogin = "camilo";
        String validPassword = "123";
        presenter.handleLoginClick(validLogin, validPassword);

        verify(view).startHomeScreen();
        verifyNoMoreInteractions(view);
    }
}